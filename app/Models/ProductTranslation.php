<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'short_description',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];
}
