<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $fillable = [
        'link',
        'status',
        'product_id',
    ];

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
