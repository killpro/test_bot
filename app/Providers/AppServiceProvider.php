<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    /**
     * @param Dispatcher $events
     */
    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {

            $event->menu->add([
                'key'           => 'lang',
                'text'          => mb_strtoupper((app()->getLocale() == 'us')?'en':app()->getLocale()),
                'url'           => '',
                'topnav_right'  => true,
            ]);

            $locales = config('app.locales');

            unset($locales[2]);

            foreach ($locales as $locale)
            {
                if($locale == app()->getLocale())
                {
                    continue;
                }
                $event->menu->addIn('lang',
                    [
                        'text'  =>  mb_strtoupper(($locale == 'us')?'en':$locale),
                        'url'   =>  route('locale',$locale),

                    ]);
            }
        });
    }
}
