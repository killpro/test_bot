<?php namespace App\Http\Requests\Backend\Product;

use Illuminate\Foundation\Http\FormRequest;


/**
 * Class ProductCreateRequest
 * @package App\Http\Requests\Product
 */
class ProductCreateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'slug'     => 'required|unique:products,slug',
            'price'    => 'required|numeric|min:0',
			'position'  => 'required|numeric',
			'image'		=> 'required',
			'count'		=> 'required',
        ];

        $languageRules = [
            'title'            => 'required',//'required',
            'description'      => 'required',
            'meta_keywords'    => '',
            'meta_title'       => '',
            'meta_description' => '',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
