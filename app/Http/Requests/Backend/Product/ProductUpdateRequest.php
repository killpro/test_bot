<?php namespace App\Http\Requests\Backend\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $product = $this->route()->parameter('product');

        $rules = [
            'slug'     => 'required|unique:products,slug,'.$product->id.',id',
            'price'    => 'required|numeric|min:0',
            'position'  => 'required|numeric',
            'count'		=> 'required',
        ];

        $languageRules = [
            'title'            => 'required',
            'description'      => 'required',
            'meta_keywords'    => '',
            'meta_title'       => '',
            'meta_description' => '',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
