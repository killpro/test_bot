<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::visible()->positionSorted()->orderBy('created_at','DESC')->get();

        return view('welcome',['products' => $products]);
    }
}
