<?php

namespace App\Http\Controllers\Backend;

use App\Api\v1\Events\Product\Create as ProductCreateEvent;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Product\ProductCreateRequest;
use App\Http\Requests\Backend\Product\ProductUpdateRequest;
use App\Models\Attribute;
use App\Models\AttributeProduct;
use App\Models\Category;
use App\Models\PackType;
use App\Models\Product;
use App\Models\ProductImages;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    use AjaxFieldsChangerTrait;

    public $module = "product";

    public function index(Request $request)
    {
        abort_unless(\Gate::allows($this->module . '_access'), 403);

        if ($request->get('draw')) {
            $list = Product::joinTranslations()->select(
                'products.id',
                'product_translations.title',
                'position',
                'status',
                'is_popular',
                'image',
                'price',
            );
            return $this->_datatable($list);
        }

        return view('admin.view.'.$this->module .'.index');
    }

    public function create()
    {
        abort_unless(\Gate::allows($this->module . '_create'), 403);

        $data['module'] = $this->module;

        $data['model'] = new Product();

        return view('admin.view.'.$this->module .'.create',$data);
    }

    public function store(ProductCreateRequest $request)
    {
        abort_unless(\Gate::allows($this->module . '_create'), 403);

        $url = $this->save_image($request,'image');

        $input = $request->all();

        $input['image'] = $url;

        $model = new Product($input);

        $model->save();

        toastr()->success(__('admin_labels.success.add',['model' => ucfirst($this->module)]));

        return redirect()->route('admin.'.$this->module .'.index');
    }

    public function edit(Product $product)
    {
        abort_unless(\Gate::allows($this->module . '_edit'), 403);

        $model = Product::findOrFail($product->id);

        $data['module'] = $this->module;

        $data['model'] = $model;

        return view('admin.view.'.$this->module .'.edit',$data);
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        abort_unless(\Gate::allows($this->module . '_edit'), 403);

        $model = Product::findOrFail($product->id);

        $input = $request->all();

        if((!$request->hasFile('image') && $product->image != null)){

            $input['image'] = $product->image;
        }else{
            $url = $this->save_image($request,'image');

            $input['image'] = $url;
        }

        $model->update($input);

        toastr()->success(__('admin_labels.success.update',['model' => ucfirst($this->module)]));

        return redirect()->route('admin.'.$this->module .'.index');
    }

    public function show(Product $product)
    {
        abort_unless(\Gate::allows($this->module . '_show'), 403);

        $this->edit($product);

    }

    public function destroy(Product $product)
    {
        abort_unless(\Gate::allows($this->module . '_delete'), 403);

        $product->delete();

        toastr()->success(__('admin_labels.success.delete',['model' => ucfirst($this->module)]));

        return back();
    }

    private function _datatable(Builder $list)
    {
        return $dataTables = DataTables::of($list)
            ->filterColumn(
                'id',
                function ($query, $keyword) {
                    $query->whereRaw("products.id like ?", ["%{$keyword}%"]);
                })
            ->filterColumn(
                'title',
                function ($query, $keyword) {
                    $query->whereRaw("product_translations.title like ?", ["%{$keyword}%"]);
                })
            ->editColumn(
                'status',
                function ($model) {
                    return view(
                        'datatables.toggler',
                        ['model' => $model, 'type' => $this->module, 'field' => 'status']
                    )->render();
                }
            )
            ->editColumn(
                'image',
                function ($model) {
                    return view('admin.partials.image',
                        ['src'=>$model->image]
                    )->render();
                }
            )
            ->addColumn(
                'actions',
                function ($model) {
                    return view(
                        'datatables.control_buttons',
                        ['model' => $model, 'front_link' => true, 'type' => $this->module]
                    )->render();
                }
            )
            ->rawColumns(['status','image', 'actions'])
            ->make();
    }
}
