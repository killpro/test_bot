<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\User\UserCreateRequest;
use App\Http\Requests\Backend\User\UserUpdateRequest;
use App\Http\Requests\Backend\User\PasswordChange;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public $module = "user";

    public function index(Request $request)
    {
        abort_unless(\Gate::allows($this->module . '_access'), 403);

        if ($request->get('draw')) {
            $list = User::with('roles')->select(
                [
                    'id',
                    'email'
                ]
            );

            return $this->_datatable($list);
        }

        return view('admin.view.'.$this->module .'.index');
    }

    public function create()
    {
        abort_unless(\Gate::allows($this->module . '_create'), 403);

        $data['model'] = new User();

        $data['without_password_change'] = false;

        $data['module'] = $this->module;

        $roles = Role::all()->pluck('title', 'id');

        return view('admin.view.'.$this->module .'.create', compact('roles'),$data);
    }

    public function store(UserCreateRequest $request)
    {
        abort_unless(\Gate::allows($this->module . '_create'), 403);

        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.'.$this->module .'.index');
    }

    public function edit(User $user)
    {
        abort_unless(\Gate::allows($this->module . '_edit'), 403);

        $model = User::findOrFail($user->id);

        $roles = Role::all()->pluck('title', 'id');

        $user->load('roles');

        $data['module'] = $this->module;

        $data['without_password_change'] = false;

        $data['model'] = $model;

        return view('admin.view.'.$this->module .'.edit', compact('roles', 'user'),$data);
    }

    public function update(UserUpdateRequest $request, User $user)
    {

        abort_unless(\Gate::allows($this->module . '_edit'), 403);
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));

        return redirect()->route('admin.'.$this->module .'.index');
    }

    public function show(User $user)
    {
        abort_unless(\Gate::allows($this->module . '_show'), 403);

        $user->load('roles');

        $data['without_password_change'] = false;

        return view('admin.view.'.$this->module .'.show', compact('user'));
    }

    public function destroy(User $user)
    {
        abort_unless(\Gate::allows($this->module . '_delete'), 403);

        $user->delete();

        return back();
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function getNewPassword($id)
    {
        $data['model'] = User::find($id);

        $data['module'] = $this->module;

        $data['without_password_change'] = false;

        if (!$data['model']) {

            return redirect()->route('admin.user.index');
        }

        return view('admin.view.'.$this->module .'.new_password',$data);
    }

    /**
     * @param                       $id
     * @param PasswordChange $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postNewPassword($id, PasswordChange $request)
    {
        $user = User::find($id);

        $user->update(['password' => Hash::make($request->input('password'))]);

        auth()->logout();

        return redirect()->route('admin.user.edit', $id);
    }

    private function _datatable(Builder $list)
    {
            $roles = $list->get();
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('users.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view('admin.view.'.
                            $this->module.'.partials.control_buttons',
                            ['model' => $model, 'type' => 'user', 'without_delete' => false]
                        )->render();
                    }
                )
                ->addColumn(
                    'roles_id',
                    function ($roles) {
                        return view('admin.view.'.
                            $this->module.'.partials.roles',
                            ['list' => $roles,'type' => 'user']
                        )->render();
                    }
                )
                ->rawColumns(['actions','roles_id'])
                ->make();
    }
}
