<?php

namespace App\Http\Controllers;

use Buglinjo\LaravelWebp\Webp;
use Exception;
use FlashMessages;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $request
     * @param $field
     * @return string|null
     */
    public function save_image($request, $field): ?string
    {
        $image = null;
        $path = null;
        if ($request->hasFile($field)) {
            $image = $request->file($field);
            $file_name = 'images'.'/'.$this->module.'/'.time().'.webp';
            $save_path = public_path('/storage/'.$file_name);
            $fileName   = time()  . '.'. $image->getClientOriginalExtension();
            /*$img = Image::make($image->getRealPath());
            $img->stream();*/
        }
        if ($image) {
            $path = 'images/' . $this->module/*.'/'.$fileName*/
            ;
            if (!file_exists($path)) {
                $path = Storage::disk('public')->put($path, $image, 'public');
            }

            return '/storage/'.$path;
        }

        return $path;
    }


    public function save_images($image){
        if ($image) {
            $_image = $image['link'];
            $fileName = Carbon::now()->format('Y-m-d--h:i:s') .'-'. $_image->getClientOriginalName()/*.'.' . $_image->getClientOriginalExtension()*/;
            $img = Image::make($_image->getRealPath());
            $img->stream();
            $path = 'images/' . $this->module . '/slider/' . $fileName;
            if (!file_exists($path)) {
                Storage::disk('public')->put($path, $img, 'public');
            }
            return Storage::url($path);
        }
        $path = null;
        return $path;
    }

}
