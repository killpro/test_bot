<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <body>
    <div class="row">
        @foreach($products as $product)
        <div class="col-sm-3 col-xs-3">
            <img src="{{asset($product->image)}}" alt="">
            <div><h2>{{__('admin_labels.title').': '.$product->title}}</h2></div>
            <div><h3>{{__('admin_labels.description').': '.$product->description}}</h3></div>
            <div>{{__('admin_labels.price').': '.$product->price}}</div>
        </div>
        @endforeach
    </div>
    </body>
</html>
