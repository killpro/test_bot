 <div class="form-group">
            <label for="image" class="col-sm-2 control-label">
                {!! Form::label('image', trans('admin_labels.image'), array('class' => "control-label")) !!}
            </label>
            <div class="col-xs-12 col-sm-7 col-md-10 col-lg-3">
                <div id="upload_div" style="display: flex; flex-direction: row-reverse;justify-content: flex-end;">
                    @if(isset($model->image))
                        <label id="delete_image" class="delete_image" style="color: red; display: block; margin-left: 10px;"
                               data-id="{!! $model->id !!}"
                               data-token="{!! csrf_token() !!}"
                               data-field="image"
                               data-url="{!! route('admin.product.ajax_field', ['id' => $model->id]) !!}"
                               data-value=''>
                            X
                        </label>
                    @endif
                    <img id="img" src="{{isset($model->image)?$model->image:asset('storage/images/upload_image.png')}}"
                         class="img-thumbnail mb-1 lg-1">
                </div>
                <input id = "input_image" class="input-file" type="file" name="image" accept="image/*">
            </div>
        </div>

<div class="form-group">
    <label for="slug" class="col-sm-2 control-label">
        {!! Form::label('slug', __('admin_labels.slug'), array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-6">
            {!! Form::text('slug', $model->slug, array('placeholder' => __('admin_labels.slug'), 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('slug', '<span class="error">:message</span>') !!}
        <br>
        <p>
            <button type="button"
                    class="btn btn-success btn-sm slug-generate">{!! __('admin_labels.buttons.generate') !!}</button>
        </p>
    </div>
</div>

        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">
                {!! Form::label('status', __('admin_labels.status'), array('class' => "control-label")) !!}
            </label>

            <div class="col-sm-10">
                <div class="col-xs-2">
                    {!! Form::select('status', array("1" => __('admin_labels.visible'), "0" => __('admin_labels.no_visible')), $model->status, array('class' => 'form-control col-xs-1')) !!}
                </div>
                {!! $errors->first('status', '<span class="error">:message</span>') !!}
            </div>
        </div>

<div class="form-group">
    <label for="position" class="col-sm-2 control-label">
        {!! Form::label('position', __('admin_labels.position'),array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-1">
            {!! Form::text('position', $model->position ? $model->position : 0, array('placeholder' => __('admin_labels.position'), 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('position', '<span class="error">:message</span>') !!}
    </div>
</div>

<div class="form-group">
    <label for="price" class="col-sm-2 control-label">
        {!! Form::label('price', __('admin_labels.price'), array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-6">
            {!! Form::number('price', $model->price, array('placeholder' => __('admin_labels.price'),'step' => 0.1, 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('price', '<span class="error">:message</span>') !!}
    </div>
</div>

<div class="form-group">
    <label for="count" class="col-sm-2 control-label">
        {!! Form::label('count', __('admin_labels.count'), array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-6">
            {!! Form::number('count', $model->count, array('placeholder' => __('admin_labels.count'),'step' => 1, 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('count', '<span class="error">:message</span>') !!}
    </div>
</div>
