<div class="form-group">
    <label for="image" class="col-sm-2 control-label">
        {!! Form::label('image', trans('admin_labels.image'), array('class' => "control-label")) !!}
    </label>
    <div class="col-xs-12 col-sm-7 col-md-10 col-lg-2">
        <img src="{{isset($model->image)?$model->image:asset('storage/images/upload_image.png')}}" id="preview" class="img-thumbnail mb-1 lg-1">
        <input class="input-file" type="file" name="image" accept="image/*">
    </div>
</div>

<div class="form-group">
    <label for="url" class="col-sm-2 control-label">
        {!! Form::label('url', __('admin_labels.url'), array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-6">
            {!! Form::text('url', $model->url, array('placeholder' => __('admin_labels.url'), 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('url', '<span class="error">:message</span>') !!}
    </div>
</div>

<div class="form-group">
    <label for="status" class="col-sm-2 control-label">
        {!! Form::label('status', __('admin_labels.status'), array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-2">
            {!! Form::select('status', array("1" => __('admin_labels.visible'), "0" => __('admin_labels.no_visible')), $model->status, array('class' => 'form-control col-xs-1')) !!}
        </div>
        {!! $errors->first('status', '<span class="error">:message</span>') !!}
    </div>
</div>

<div class="form-group">
    <label for="position" class="col-sm-2 control-label">
        {!! Form::label('position', __('admin_labels.position'),array('class' => "control-label")) !!}
    </label>

    <div class="col-sm-10">
        <div class="col-xs-1">
            {!! Form::text('position', $model->position ? $model->position : 0, array('placeholder' => __('admin_labels.position'), 'class' => 'form-control input-sm')) !!}
        </div>
        {!! $errors->first('position', '<span class="error">:message</span>') !!}
    </div>
</div>
