<?php

use Illuminate\Support\Facades\Route;

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
})->name('locale');
Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.'
    ],
    function ()
    {
        Route::group(
            [
                'middleware' => 'auth',
            ],
            function () {
                Route::get(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.get', 'uses' => 'UserController@getNewPassword']
                );
                Route::post(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.post', 'uses' => 'UserController@postNewPassword']
                );
                Route::resource('user', 'UserController', ['except' => 'delete']);
                Route::resource('permissions', 'PermissionsController');
                Route::resource('roles', 'RolesController');

                //products
                Route::post('product/{id}/ajax_field','ProductController@ajaxFieldChange')->middleware('ajax')->name('product.ajax_field');
                Route::resource('product', 'ProductController');

                // translations
                Route::get(
                    'translation/{group}',
                    ['as' => 'translation.index', 'uses' => 'TranslationController@index']
                );
                Route::post(
                    'translation/{group}',
                    ['as' => 'translation.update', 'uses' => 'TranslationController@update']
                );
            });


        Route::get('login', 'AuthController@showLoginForm')->name('login');
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::get('/','HomeController@index')->name('home');
    }
);
